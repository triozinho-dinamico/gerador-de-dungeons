package graph;

import javax.annotation.Nullable;

public class DepthFirstTraversal extends TraversalStrategyInterface
{
    StringBuilder traversedPath;

    public DepthFirstTraversal(AbstractGraph g)
    {
        super(g);
        traversedPath = new StringBuilder();
    }

    @Override
    public void traverseGraph(Vertex source, @Nullable Vertex destination)
    {
        depthFirstTraversalRecursion(source);
        printPath();
    }

    private void depthFirstTraversalRecursion(Vertex source)
    {
        markVertexAsVisited(getGraph().getVertices().indexOf(source));
        traversedPath.append(source).append('\n');
        addToPath(source);
        Vertex adjacentVertex = getGraph().getFirstConnectedVertex(source);
        while(adjacentVertex != null)
        {
            int adjacentVertexIndex = getGraph().getVertices().indexOf(adjacentVertex);

            if(!hasVertexBeenVisited(adjacentVertexIndex))
            {
                updateTraversalInfoForVertex(adjacentVertexIndex, getGraph().getVertices().indexOf(source));
                depthFirstTraversalRecursion(adjacentVertex);
            }
            adjacentVertex = getGraph().getNextConnectedVertex(source, adjacentVertex);

        }
    }
    
    private void updateTraversalInfoForVertex(int newVertexIndex, int previousVertexIndex)
    {
        float distance;
                
        Vertex newVertex = getGraph().getVertices().get(newVertexIndex);
        Vertex oldVertex = getGraph().getVertices().get(previousVertexIndex);
        float newDistance = ((float)((Room)oldVertex).getPoint().distance(((Room)newVertex).getPoint()));
        if(getDistanceToVertex(previousVertexIndex) == Float.POSITIVE_INFINITY)
            distance = newDistance;
        else
            distance = getDistanceToVertex(previousVertexIndex) + newDistance;
        markVertexAsVisited(newVertexIndex);
        setDistanceToVertex(newVertexIndex, distance);
        setPredecessorVertexIndex(newVertexIndex, previousVertexIndex);
        setSuccessorVertexIndex(previousVertexIndex, newVertexIndex);
    }

}
