/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graph;

/**
 *
 * @author iaraduarte
 */
public class GraphConverter 
{
    public static AbstractGraph predecessorListToGraph(AbstractGraph dungeon, int[] predecessorArray)
    {
        AbstractGraph cleanDungeon = dungeon;
        
        cleanDungeon.removeAllEdges();
        
        int vertexIterator=0;
        int iterator=0;
        
        int numberRooms = cleanDungeon.getNumberOfVertices();
        
        while(vertexIterator<numberRooms)
        {
            while(iterator<numberRooms)
            {
                if(predecessorArray[iterator]==vertexIterator)
                {
                    float distance = getDistanceBetweenPoint(cleanDungeon.getVertices().get(iterator), cleanDungeon.getVertices().get(vertexIterator));
                    cleanDungeon.addEdge(cleanDungeon.getVertices().get(iterator), cleanDungeon.getVertices().get(vertexIterator),distance);
                }
                iterator++;
            }
            vertexIterator++;
            iterator=0;
        }
        return cleanDungeon;
    }
    
    public static float getDistanceBetweenPoint(Vertex source, Vertex destination)
    {
        double distanceX = ((Room)source).getPoint().getX() - ((Room)destination).getPoint().getX();
        double distanceY = ((Room)source).getPoint().getY() - ((Room)destination).getPoint().getY();
        
        double sumPoint = Math.pow(distanceX, 2)+Math.pow(distanceY, 2);
        
        double result = Math.sqrt(sumPoint);
        
        return (float)result;
    }
}
